-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 11/08/2018 às 15:18
-- Versão do servidor: 5.7.23-0ubuntu0.16.04.1
-- Versão do PHP: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `belize`
--
CREATE DATABASE IF NOT EXISTS `belize2` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `belize2`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `canal`
--

DROP TABLE IF EXISTS `canal`;
CREATE TABLE `canal` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  `descricao` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `id_workspace` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `canal`
--

INSERT INTO `canal` (`id`, `nome`, `descricao`, `id_workspace`) VALUES
(1, 'teste_1', 'teste_1', 1),
(2, 'teste_2', 'teste_2', 1),
(3, 'teste_3', 'teste_3', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `mensagem`
--

DROP TABLE IF EXISTS `mensagem`;
CREATE TABLE `mensagem` (
  `id` int(11) NOT NULL,
  `texto` varchar(3000) COLLATE utf8_bin NOT NULL,
  `data_hora_envio` datetime NOT NULL,
  `data_hora_leitura` datetime DEFAULT NULL,
  `id_remetente` int(11) NOT NULL,
  `id_canal_destinatario` int(11) DEFAULT NULL,
  `id_usuario_destinatario` int(11) DEFAULT NULL,
  `id_msg_citada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `mensagem`
--

-- INSERT INTO `mensagem` (`id`, `texto`, `data_hora_envio`, `data_hora_leitura`, `id_remetente`, `id_canal_destinatario`, `id_usuario_destinatario`, `id_msg_citada`) VALUES
-- (1, 'Olá, boa tarde!', '2018-04-21 00:00:00', NULL, 3, 1, NULL, NULL),
-- (2, 'Vocês estão aí?', '2018-04-21 00:00:00', NULL, 3, 1, NULL, NULL),
-- (3, 'Olá! Td bem, João?', '2018-04-21 00:00:00', NULL, 10, 5, NULL, NULL),
-- (4, 'Ola usuario 1!', '2018-04-21 00:00:00', NULL, 3, NULL, 1, NULL),
-- (5, 'Bom dia, usuario 1!', '2018-04-21 00:00:00', NULL, 10, NULL, 1, NULL),
-- (6, 'teste', '2018-04-22 19:54:05', NULL, 1, 1, NULL, NULL),
-- (7, 'teste', '2018-04-22 19:57:46', NULL, 1, 1, NULL, NULL),
-- (8, 'oi, foi', '2018-04-22 20:03:14', NULL, 1, 1, NULL, NULL),
-- (9, 'Teste', '2018-04-22 20:06:33', NULL, 1, NULL, 10, NULL),
-- (10, 'olá mundo fim', '2018-07-01 16:30:45', NULL, 1, 1, NULL, NULL),
-- (11, 'olá mundo 12', '2018-07-01 16:36:23', NULL, 1, 1, NULL, NULL),
-- (12, '123', '2018-07-01 16:36:54', NULL, 1, 1, NULL, NULL),
-- (13, '123', '2018-07-01 16:44:17', NULL, 1, NULL, 1, NULL),
-- (14, 'msg para ana', '2018-07-01 16:44:40', NULL, 1, NULL, 10, NULL),
-- (15, 'oi', '2018-07-03 21:44:25', NULL, 1, 1, NULL, NULL),
-- (16, 'wer', '2018-07-03 21:44:54', NULL, 1, 1, NULL, NULL),
-- (17, '123', '2018-07-03 21:51:03', NULL, 1, 1, NULL, NULL),
-- (18, 'wsws', '2018-07-03 21:51:27', NULL, 1, 5, NULL, NULL),
-- (19, 'swsw', '2018-07-03 21:51:33', NULL, 1, NULL, 1, NULL),
-- (20, 'd', '2018-07-03 21:53:27', NULL, 1, 5, NULL, NULL),
-- (21, 'undefined', '2018-07-03 21:53:46', NULL, 1, 1, NULL, NULL),
-- (22, 'd', '2018-07-03 22:11:45', NULL, 1, 5, NULL, NULL),
-- (23, 'xs', '2018-07-03 22:12:50', NULL, 1, 1, NULL, NULL),
-- (24, 'sd', '2018-07-03 22:23:45', NULL, 1, NULL, 1, NULL),
-- (25, 'teftsft', '2018-07-03 22:29:31', NULL, 1, NULL, 1, NULL),
-- (26, 'gyasbjs', '2018-07-03 22:29:51', NULL, 1, 1, NULL, NULL),
-- (27, 'huahuahua', '2018-07-03 22:30:02', NULL, 1, NULL, 1, NULL),
-- (28, 'teste', '2018-07-03 22:30:37', NULL, 1, 1, NULL, NULL),
-- (29, 'tesre', '2018-07-03 22:36:53', NULL, 1, 1, NULL, NULL),
-- (30, 'hui', '2018-07-03 22:44:51', NULL, 1, 1, NULL, NULL),
-- (31, 'teste', '2018-07-03 22:47:07', NULL, 1, 1, NULL, NULL),
-- (32, 'erw', '2018-07-03 22:47:17', NULL, 1, 1, NULL, NULL),
-- (33, 'ola', '2018-07-03 22:49:28', NULL, 1, 1, NULL, NULL),
-- (34, 'ert', '2018-07-03 22:53:21', NULL, 1, 1, NULL, NULL),
-- (35, 'teste', '2018-07-03 22:54:14', NULL, 1, 1, NULL, NULL),
-- (36, 'ers', '2018-07-03 22:54:32', NULL, 1, 5, NULL, NULL),
-- (37, '1234', '2018-07-04 00:28:18', NULL, 1, 1, NULL, NULL),
-- (38, 'teste', '2018-07-04 00:28:59', NULL, 1, 1, NULL, NULL),
-- (39, '1233', '2018-07-04 00:30:05', NULL, 1, 1, NULL, NULL),
-- (40, 'mais uma', '2018-07-31 00:00:00', NULL, 3, 1, NULL, NULL),
-- (41, 'mais uma', '2018-07-31 00:00:00', NULL, 3, 1, NULL, NULL),
-- (42, 'teste', '2018-07-31 18:18:03', NULL, 1, 1, NULL, NULL),
-- (43, 'teste', '2018-07-31 18:18:03', NULL, 1, 1, NULL, NULL),
-- (44, 'teste', '2018-07-31 18:18:03', NULL, 3, 1, NULL, NULL),
-- (45, 'ha', '2018-08-07 11:39:33', NULL, 1, 1, NULL, NULL),
-- (46, 'teste', '2018-08-07 11:39:53', NULL, 1, NULL, 3, NULL),
-- (47, 'er', '2018-08-07 11:40:26', NULL, 1, 1, NULL, NULL),
-- (48, 'oi', '2018-08-07 11:41:45', NULL, 1, 5, NULL, NULL),
-- (49, 'hallo', '2018-08-07 11:41:54', NULL, 1, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipo_reacao`
--

DROP TABLE IF EXISTS `tipo_reacao`;
CREATE TABLE `tipo_reacao` (
  `id` int(11) NOT NULL,
  `nome` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `tipo_reacao`
--

INSERT INTO `tipo_reacao` (`id`, `nome`) VALUES
(1, 'POSITIVA'),
(2, 'NEGATIVA'),
(3, 'NEUTRA');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL,
  `sobrenome` varchar(45) COLLATE utf8_bin NOT NULL,
  `login` varchar(20) COLLATE utf8_bin NOT NULL,
  `senha` varchar(45) COLLATE utf8_bin NOT NULL,
  `img` varchar(300) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `sobrenome`, `login`, `senha`, `img`) VALUES
(1, 'João', 'Guinelli', 'jvguinelli', '1234', ''),
(2, 'Matheus', 'Fagundes', 'matheusf', '1234', ''),
(3, 'Octavio', 'Vieira', 'octaviov', '1234', ''),
(4, 'Renato', 'Pereira', 'renatopereira', '1234', ''),
(5, 'Andre', 'Rosa', 'andrerosa', '1234', ''),
(6, 'Príncia', 'Dionízio', 'princiad', '1234', ''),
(7, 'Willian', 'Gonçalves', 'williangoncalves', '1234', ''),
(8, 'Pablo', 'Veiga', 'pabloveiga', '1234', ''),
(9, 'Marcela', 'Barboza', 'marcelabarboza', '1234', ''),
(10, 'Leonam', 'Medeiros', 'leonammedeiros', '1234', ''),
(11, 'Marisa', 'Guinelli', 'marisaguinelli', '1234', ''),
(12, 'Desconhecido', 'Nao Informado', 'desconhecido', '1234', '');


-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_canal`
--

DROP TABLE IF EXISTS `usuario_canal`;
CREATE TABLE `usuario_canal` (
  `id` int(11) NOT NULL,
  `data_de_entrada` date NOT NULL,
  `data_de_saida` date DEFAULT NULL,
  `data_hora_ultima_visualizacao` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_canal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `usuario_canal`
--

INSERT INTO `usuario_canal` (`id`, `data_de_entrada`, `data_de_saida`, `data_hora_ultima_visualizacao`, `id_usuario`, `id_canal`) VALUES
(1, '2018-04-20', NULL, '2018-04-20 00:00:00', 1, 1),
(2, '2018-04-20', NULL, '2018-04-20 00:00:00', 3, 1),
(3, '2018-04-20', NULL, '2018-04-20 00:00:00', 5, 1),
(4, '2018-04-20', NULL, '2018-04-20 00:00:00', 7, 1),
(5, '2018-04-20', NULL, '2018-04-20 00:00:00', 2, 2),
(6, '2018-04-20', NULL, '2018-04-20 00:00:00', 4, 2),
(7, '2018-04-20', NULL, '2018-04-20 00:00:00', 6, 2),
(8, '2018-04-20', NULL, '2018-04-20 00:00:00', 8, 2),
(9, '2018-04-20', NULL, '2018-04-20 00:00:00', 1, 3),
(10, '2018-04-20', NULL, '2018-04-20 00:00:00', 2, 3),
(11, '2018-04-20', NULL, '2018-04-20 00:00:00', 3, 3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_mensagem`
--

DROP TABLE IF EXISTS `usuario_mensagem`;
CREATE TABLE `usuario_mensagem` (
  `id` int(11) NOT NULL,
  `id_tipo_reacao` int(11) NOT NULL,
  `id_mensagem` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_workspace`
--

DROP TABLE IF EXISTS `usuario_workspace`;
CREATE TABLE `usuario_workspace` (
  `id` int(11) NOT NULL,
  `data_de_entrada` date NOT NULL,
  `data_de_saida` date DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_workspace` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `usuario_workspace`
--

INSERT INTO `usuario_workspace` (`id`, `data_de_entrada`, `data_de_saida`, `id_usuario`, `id_workspace`) VALUES
(1, '2018-04-20', NULL, 1, 1),
(2, '2018-04-20', NULL, 2, 1),
(3, '2018-04-20', NULL, 3, 1),
(4, '2018-04-20', NULL, 4, 1),
(5, '2018-04-20', NULL, 5, 1),
(6, '2018-04-20', NULL, 6, 1),
(7, '2018-04-20', NULL, 7, 1),
(8, '2018-04-20', NULL, 8, 1),
(9, '2018-04-20', NULL, 9, 1),
(10, '2018-04-20', NULL, 10, 1),
(11, '2018-04-20', NULL, 11, 2),
(12, '2018-04-20', NULL, 12, 2),
(13, '2018-04-20', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `workspace`
--

DROP TABLE IF EXISTS `workspace`;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `descricao` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `workspace`
--

INSERT INTO `workspace` (`id`, `nome`, `descricao`) VALUES
(1, 'workspace 01', 'teste de workspace'),
(2, 'workspace 02', 'teste workspace 02');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `canal`
--
ALTER TABLE `canal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_canal_1_idx` (`id_workspace`);

--
-- Índices de tabela `mensagem`
--
ALTER TABLE `mensagem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mensagem_1_idx` (`id_remetente`) USING BTREE,
  ADD KEY `fk_mensagem_2_idx` (`id_canal_destinatario`) USING BTREE,
  ADD KEY `fk_mensagem_3_idx` (`id_usuario_destinatario`),
  ADD KEY `fk_mensagem_4_idx` (`id_msg_citada`) USING BTREE;

--
-- Índices de tabela `tipo_reacao`
--
ALTER TABLE `tipo_reacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Índices de tabela `usuario_canal`
--
ALTER TABLE `usuario_canal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_canal_ator_2_idx` (`id_canal`),
  ADD KEY `fk_usuario_canal_1_idx` (`id_usuario`);

--
-- Índices de tabela `usuario_mensagem`
--
ALTER TABLE `usuario_mensagem`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `un_reacao` (`id_mensagem`,`id_usuario`),
  ADD KEY `fk_reacao_1_idx` (`id_mensagem`),
  ADD KEY `fk_reacao_2_idx` (`id_usuario`),
  ADD KEY `fk_reacao_3_idx` (`id_tipo_reacao`);

--
-- Índices de tabela `usuario_workspace`
--
ALTER TABLE `usuario_workspace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_workspace_1_idx` (`id_usuario`),
  ADD KEY `fk_usuario_workspace_2_idx` (`id_workspace`);

--
-- Índices de tabela `workspace`
--
ALTER TABLE `workspace`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `canal`
--
ALTER TABLE `canal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `mensagem`
--
ALTER TABLE `mensagem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de tabela `tipo_reacao`
--
ALTER TABLE `tipo_reacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de tabela `usuario_canal`
--
ALTER TABLE `usuario_canal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `usuario_mensagem`
--
ALTER TABLE `usuario_mensagem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `usuario_workspace`
--
ALTER TABLE `usuario_workspace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de tabela `workspace`
--
ALTER TABLE `workspace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `canal`
--
ALTER TABLE `canal`
  ADD CONSTRAINT `fk_canal_1` FOREIGN KEY (`id_workspace`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `mensagem`
--
ALTER TABLE `mensagem`
  ADD CONSTRAINT `fk_menssagem_1` FOREIGN KEY (`id_remetente`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_menssagem_2` FOREIGN KEY (`id_canal_destinatario`) REFERENCES `canal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_menssagem_3` FOREIGN KEY (`id_usuario_destinatario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_menssagem_4` FOREIGN KEY (`id_msg_citada`) REFERENCES `mensagem` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `usuario_canal`
--
ALTER TABLE `usuario_canal`
  ADD CONSTRAINT `fk_usuario_canal_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_canal_2` FOREIGN KEY (`id_canal`) REFERENCES `canal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `usuario_mensagem`
--
ALTER TABLE `usuario_mensagem`
  ADD CONSTRAINT `fk_reacao_1` FOREIGN KEY (`id_mensagem`) REFERENCES `mensagem` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reacao_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reacao_3` FOREIGN KEY (`id_tipo_reacao`) REFERENCES `tipo_reacao` (`id`);

--
-- Restrições para tabelas `usuario_workspace`
--
ALTER TABLE `usuario_workspace`
  ADD CONSTRAINT `fk_usuario_workspace_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_workspace_2` FOREIGN KEY (`id_workspace`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
