import pymysql
import pyorient


class DBFactory(object):

    @staticmethod
    def get_conexao():
        conexao = pyorient.OrientDB("localhost", 2424)
        conexao.db_open("belize_inicial", "root", "root")
        return conexao

